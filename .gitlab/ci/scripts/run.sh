#!/bin/bash

deploy=$([ "$1" == "deploy" ] && echo 1 || echo 0);
registry="registry.git.feneas.org/feneas/infrastructure/docker-images";

function build() {
  local dir=$1;
  local version=$(git tag -l "*-${dir}*" --sort=-refname \
    | head -1 | sed "s/^\(.*\)-${dir}.*/\1/");

  if [ "$version" != "" ] && [ -f "$dir/Dockerfile" ]; then
    echo -ne "\n\nSTART BUILDING ${dir}:${version}\n\n"
    docker build -t ${registry}/${dir}:${version} ${dir} || exit 1;
    docker tag ${registry}/${dir}:${version} ${registry}/${dir}:latest || exit 1;
    if [ "$deploy" == "1" ]; then
      echo -ne "\n\nDEPLOYING ${dir}:${version}\n\n"
      docker push ${registry}/${dir}:${version} || exit 1;
      docker push ${registry}/${dir}:latest || exit 1;
    fi
  fi
}

if [ "$CI_COMMIT_TAG" == "" ]; then
  for directory in $(ls); do
    if [ -d "$directory" ]; then
      build "$directory";
    fi
  done
else
  build "$(echo $CI_COMMIT_TAG | cut -d- -f2- | cut -d. -f1)";
fi
