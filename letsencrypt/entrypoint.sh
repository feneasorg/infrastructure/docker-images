#!/bin/sh

# expand docker secrets
. /env_secrets_expand.sh

/usr/bin/rancher-letsencrypt $@
