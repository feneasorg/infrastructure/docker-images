package main

import (
  "os"
  "io/ioutil"

  "github.com/rs/zerolog"
  customLog "github.com/gin-contrib/logger"
  "github.com/gin-gonic/gin"
)

var logger = zerolog.New(os.Stdout).
  Output(zerolog.ConsoleWriter{Out: os.Stdout}).
  With().
  Stack().
  Caller().
  Timestamp().
  Logger()

func main() {
  router := gin.New()
  router.Use(customLog.SetLogger(customLog.Config{Logger: &logger}))

  router.Any("/*catchall", func(ctx *gin.Context) {
    body, err := ioutil.ReadAll(ctx.Request.Body)
    defer ctx.Request.Body.Close()

    logger.Info().Err(err).Msgf("Request to %s %s\n\nHeader:\n%+v\n\nBody:\n%+v\n",
      ctx.Request.Method,
      ctx.Request.URL.String(),
      ctx.Request.Header,
      string(body))
  })

  logger.Info().Msgf("Listening on localhost:9000 ..",)
  logger.Fatal().Err(router.Run("localhost:9000")).Msg("cannot bind listening interface")
}
