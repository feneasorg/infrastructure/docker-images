module sandbox

go 1.14

require (
	github.com/gin-contrib/logger v0.0.2 // indirect
	github.com/gin-gonic/gin v1.6.2 // indirect
	github.com/rs/zerolog v1.18.0 // indirect
)
