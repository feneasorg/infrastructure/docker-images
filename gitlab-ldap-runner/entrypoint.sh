#!/bin/bash

/etc/init.d/slapd start
/etc/init.d/ssh start

# update ssh key in case it was changed
cp -v /home/gitlab-runner/.ssh/id_rsa.pub /root/.ssh/authorized_keys

/usr/bin/gitlab-runner $@
