# Custom Gitlab Runner

The member repository requires a custom gitlab runner  
with pre installed ansible and configured shell executor.

## Configuration

    concurrent = 5
    check_interval = 0
    
    [[runners]]
      name = "restricted-runner"
      url = "https://git.feneas.org/"
      token = "XXXXXXXXXXXXXXXXXXXXXXXXX"
      executor = "shell"
      [runners.cache]

## Docker

e.g.

    docker build -t zauberstuhl/gitlab-ansible-runner .
    socker push zauberstuhl/gitlab-ansible-runner

## Gitlab

Start the runner image and configure the gitlab  
repository as private runner. Disable shared runners!
