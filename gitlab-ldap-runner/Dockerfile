FROM golang:1.14

ADD http-sandbox /go/src/http-sandbox
RUN cd /go/src/http-sandbox && go build -o /tmp/http-sandbox

FROM gitlab/gitlab-runner:v13.0.1

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install openssh-server ldap-server ldap-utils python-pip python-ldap -y
RUN apt-get clean && apt-get autoclean
RUN pip install ansible

# install debugging tool for web requests
COPY --from=0 /tmp/http-sandbox /usr/bin/http-sandbox
RUN chmod +x /usr/bin/http-sandbox

# patching https://github.com/ansible/ansible/pull/44475
#ADD ldap_passwd.patch /tmp/ldap_passwd.patch
#RUN patch -p1 </tmp/ldap_passwd.patch && rm /tmp/ldap_passwd.patch

# configure ldap for testing environment
ADD *.ldif /tmp/
RUN rm -r /var/lib/ldap && mkdir -p /var/lib/ldap
RUN rm -r /etc/ldap/slapd.d && mkdir -p /etc/ldap/slapd.d
RUN slapadd -n 0 -F /etc/ldap/slapd.d -l /tmp/config.ldif
RUN slapadd -n 1 -F /etc/ldap/slapd.d -l /tmp/data.ldif
RUN rm /tmp/config.ldif /tmp/data.ldif
RUN chown -R openldap:openldap /etc/ldap/slapd.d
RUN chown -R openldap:openldap /var/lib/ldap

# configure ssh for testing environment
RUN su gitlab-runner -c 'ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa' && \
  su gitlab-runner -c \
    'echo "Host *\n  StrictHostKeyChecking no" > ~/.ssh/config' && \
  mkdir -p /root/.ssh && \
  cp /home/gitlab-runner/.ssh/id_rsa.pub /root/.ssh/authorized_keys

ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["run", "--user=gitlab-runner", "--working-directory=/home/gitlab-runner"]
