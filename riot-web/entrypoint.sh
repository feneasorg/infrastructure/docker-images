#!/bin/bash

wwwdir="/var/www/html"
[[ "$VERSION" == "" ]] && VERSION="v1.0.1"

echo "Save config"
cp $wwwdir/config.json /tmp

echo "Updating to $VERSION"
rm -r /var/www/html/* > /dev/null
curl --progress -LO \
  https://github.com/vector-im/riot-web/releases/download/$VERSION/riot-$VERSION.tar.gz
tar -x -f riot-$VERSION.tar.gz
mv riot-$VERSION/* .
rm -r riot-$VERSION*

echo "Restoring config"
mv /tmp/config.json $wwwdir

echo "Patching riot-web"
# patch front page (cannot be configured through config.json)
logo=$(cat $wwwdir/config.json | grep authHeaderLogoUrl | cut -d '"' -f4)
sed -i 's#\(Riot.im":.*\)Riot.im#\1chat.feneas.org#' $wwwdir/i18n/*.json
sed -i 's#\(https://\)riot.im#\1feneas.org#' $wwwdir/welcome.html
sed -i "s#welcome/images/logo.svg#$logo#" $wwwdir/welcome.html
sed -i 's#height: 54px;#height: 200px;#' $wwwdir/welcome.html

echo "Running nginx"
/usr/sbin/nginx -g 'daemon off;' -c /etc/nginx/nginx.conf $@
