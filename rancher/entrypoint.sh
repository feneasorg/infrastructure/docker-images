#!/bin/bash

if [[ "$DOMAIN" == "" ]]; then
  echo "You have to specify a valid domain name!"
  sleep 10
  exit 1
fi

defaultconf="/etc/nginx/conf.d/default.conf"
if [[ "$ACME_MAIL" != "" ]]; then
  sed "s#<server>#${DOMAIN}#g" \
    /etc/nginx/conf.d/nginx-ssl.conf.template > $defaultconf

  # start cron
  /usr/sbin/cron
  echo "0 0,12 * * * python -c 'import random; import time; time.sleep(random.random() * 3600)' && certbot-auto renew" | crontab -

  # create a selfsigned cert for first run
  openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout /etc/ssl/private/nginx-selfsigned.key \
    -out /etc/ssl/certs/nginx-selfsigned.crt \
    -subj "/C=DE/ST=Denial/CN=www.example.com"

  /usr/bin/certbot-auto --nginx --no-bootstrap \
    -m $ACME_MAIL --agree-tos --non-interactive \
    --no-eff-email --domains $DOMAIN
else
  sed "s#<server>#${DOMAIN}#" \
    /etc/nginx/conf.d/nginx.conf.template > $defaultconf
fi

# start nginx
service nginx start

# start rancher
/usr/bin/entry $@ /usr/bin/s6-svscan /service
