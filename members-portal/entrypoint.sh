#!/bin/bash

appconf="src/git.feneas.org/feneas/infrastructure/members-portal/conf/app.conf"

if [ "$CONFIG" != "" ]; then
  echo -ne "$CONFIG" > $appconf
fi

if [ "$CONFIG_FILE" != "" ]; then
  cp -v $CONFIG_FILE $appconf
fi

./run.sh
