#!/bin/bash

while read -r line; do
  # ignore non-iptable statements
  [[ $line != iptables* ]] && continue;
  echo "Adding: $line";
  eval $line || exit 1;
done <<< "$IPTABLE_RULES"
