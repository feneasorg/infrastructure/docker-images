#!/bin/bash
# you can also use tcp/ip here, consult spamc(1)

email=${1}
parts=(${email/@/ })
user_dir=/var/mail/${parts[1]}/${parts[0]}/.Junk

exec /usr/bin/sa-learn --spam $user_dir \
  --dbpath /var/mail-state/lib-amavis/.spamassassin
